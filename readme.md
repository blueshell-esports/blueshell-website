# Blueshell Website
This repository contains the docker-compose and config files for the Blueshell website.

## Setup Instructions
0. **Install docker**: Follow the guide [here](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository) to install up Docker and Docker compose.
1. **Do a little cloning**: Clone this repository and its submodules using the `git clone --recurse-submodules` command. 
2. **Configure HTTPS**:
   - Before starting the server for the first time, comment out the 4 lines indicated in `nginx.conf`. 
   - Start the server without https using `docker compose up`. This will run Certbot to obtain an SSL certificate for esa-blueshell.nl. 
   - After you see a message in the output from certbot about successfully getting the certificate, you can quit out with control-c. 
   - Then uncomment the 4 lines in `nginx.conf` again.
3. **Profit**: Run `docker compose up -d` to start the server in the background. You can now access the website at https://esa-blueshell.nl.
4. //TODO: cronjob for refreshing the certbot certificate
## Updating the website (front or backend)
1. Update the repo using `git pull --recurse-submodules`.
2. Then just execute `docker compose down && docker compose up -d --build` and everything should be running again after building the new images.