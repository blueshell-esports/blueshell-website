echo "Starting docker containers"
docker-compose up --build

echo "Running certbot"
docker compose run --rm  certbot certonly --webroot --webroot-path /var/www/certbot/ -d esa-blueshell.nl

